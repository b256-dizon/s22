
//    1. Create a function which will allow us to register into the registeredUsers list.
//         - this function should be able to receive a string.
//         - determine if the input username already exists in our registeredUsers array.
//             -if it is, show an alert window with the following message:
//                 "Registration failed. Username already exists!"
//             -if it is not, add the new username into the registeredUsers array and show an alert:
//                 "Thank you for registering!"
//         - invoke and register a new user.
//         - outside the function log the registeredUsers array.

// */
    


let registeredUsers = [

  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
  "Conan O' Brien"
]

function registerUser(username) {
  if (registeredUsers.includes(username)) {
    alert (`${username} Registration failed. Username already in used. Please use another one.`);
  } else {
    registeredUsers.push(username);
    alert(`${username} Thank you for registering.`);

  }
}

//    1. Create a function which will allow us to register into the registeredUsers list.
//         - this function should be able to receive a string.
//         - determine if the input username already exists in our registeredUsers array.
//             -if it is, show an alert window with the following message:
//                 "Registration failed. Username already exists!"
//             -if it is not, add the new username into the registeredUsers array and show an alert:
//                 "Thank you for registering!"
//         - invoke and register a new user.
//         - outside the function log the registeredUsers array.

// */
    
let friendsList = [

  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
  "Conan O' Brien"
]

function addFriend(username) {
  if (registeredUsers.includes(username)) {
    friendsList.push(username);
    console.log(`${username} has been added to your friends list`);
  } else {
    console.log(`${username} is not a registered user`);
  }
}
/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    

function displayFriends() {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    console.log("Your friends list:");
    for (let i = 0; i < friendsList.length; i++) {
      console.log(friendsList[i]);
    }
  }
}

displayFriends();



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends() {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    alert(`You currently have ${friendsList.length} friends.`);
  }
}

displayNumberOfFriends();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function deleteLastFriend() {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.pop();
    alert("The last friend in the list has been removed.");
  }
}

deleteLastFriend(); // Invokes the function

console.log(friendsList); // Logs the updated friendsList array
